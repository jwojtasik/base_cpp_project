#pragma once

class Solution
{
public:
    explicit Solution(int x);

private:
    int x;
};